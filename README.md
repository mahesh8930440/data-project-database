# IPL Data Project Database

## DESCRIPTION:
 
 - This repository contains SQL scripts and queries to work with the IPL (Indian Premier League) dataset using PostgreSQL. The project includes tasks to create a database, load data from a CSV file, and perform various data analysis tasks using SQL queries.

## Setup :

###  Getting Started

 1. Clone this repository to your local machine.
  
  - git clone https://gitlab.com/mahesh8930440/data-project-database.git

 2. Install PostgreSQL 

  - If you haven't already installed PostgreSQL, you can do so with the following command:

     - sudo apt install postgresql postgresql-contrib

 3. Start the PostgreSQL server

  - Ensure that the PostgreSQL server is running:

     - sudo systemctl start postgresql.service

 4. Create a Database for the Project
 
  - Log in to the PostgreSQL shell as the postgres user:

     - sudo -u postgres psql

  - Create a database for the IPL Data Project.

     - CREATE DATABASE  IPLDATASET

 5. Run the SQL File

  - Navigate to the sql-queries.sql file in your terminal and execute the following command to run the SQL script:

     - sudo psql -h 127.0.0.1 -d IPLDATASET -U postgres -p 5432 -a -q -f sql-queries.sql

  - This will execute the SQL script and perform the necessary operations on the IPL data in the PostgreSQL database.

  - Please make sure to follow these steps carefully to set up and run the project successfully.    
