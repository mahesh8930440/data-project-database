-- 1. Write a SQL script that creates a new user, and database. Make the user the owner of the db.

CREATE DATABASE IPLPROJECT;

CREATE USER SQL WITH PASSWORD '1234';

ALTER DATABASE SQLDatabase OWNER TO SQL;


-- 2.Write another SQL script that cleans up the user, and database created in the previous step.

ALTER DATABASE SQLDatabase OWNER TO POSTGRES;

DROP USER SQL;

DROP DATABASE IF EXISTS  SQLDatabase;

--3. Load CSV
   
 -- Write a SQL script that loads CSV data into a table.

CREATE TABLE deliveries(match_id INT,inning INT,batting_team VARCHAR,bowling_team VARCHAR,over INT,ball INT,batsman VARCHAR,non_striker VARCHAR,bowler VARCHAR,is_super_over INT,wide_runs INT,bye_runs INT,legbye_runs INT,noball_runs INT,penalty_runs INT,batsman_runs INT,extra_runs INT,total_runs INT,player_dismissed VARCHAR,dismissal_kind VARCHAR,fielder VARCHAR)

COPY deliveries FROM '/var/lib/postgresql/deliveries.csv' DELIMITER ',' CSV HEADER;

CREATE TABLE matches(id INT,season VARCHAR,city VARCHAR,date DATE,team1 VARCHAR,team2 VARCHAR,toss_winner VARCHAR,toss_decision VARCHAR,result VARCHAR,dl_applied INT,winner VARCHAR,win_by_runs INT,win_by_wickets INT,player_of_match VARCHAR,venue VARCHAR,umpire1 VARCHAR,umpire2 VARCHAR,umpire3 VARCHAR)

COPY matches FROM '/var/lib/postgresql/matches.csv' DELIMITER ',' CSV HEADER;

--1. Number of matches played per year for all the years in IPL.

SELECT season,COUNT(season) as matchesPlayedPerSeason
FROM matches
GROUP BY season
ORDER BY season;

--2. Number of matches won per team per year in IPL.

SELECT season,Winner as teams,COUNT(Winner) as teamWonMatchesPerSeason
FROM matches
group by winner,season
ORDER BY season;

--3. Extra runs conceded per team in the year 2016

SELECT bowling_team as TEAMS,SUM(extra_runs) as extraRunsConcededPerTeamIn2016
FROM deliveries
INNER JOIN matches ON
deliveries.match_id=matches.id
GROUP BY TEAMS ,season
HAVING season=2016;

--4. Top 10 economical bowlers in the year 2015

WITH duplicateTable AS
    (SELECT deliveries.bowler AS bowler,
	            SUM((deliveries.total_runs - deliveries.legbye_runs - deliveries.bye_runs - deliveries.penalty_runs)) as runs,
	            COUNT(CASE WHEN deliveries.wide_runs = 0 AND deliveries.noball_runs = 0 THEN 1 END) as balls
            
        FROM deliveries
        INNER JOIN matches ON deliveries.match_id = matches.id
        WHERE matches.season = 2015
        GROUP BY deliveries.bowler)

SELECT duplicateTable.bowler,ROUND((duplicateTable.runs)*6.0/duplicateTable.balls,2) AS economy
FROM duplicateTable
ORDER BY economy
LIMIT 10;


--5. Find the number of times each team won the toss and also won the match

SELECT winner,COUNT(winner) as teamWonBothTossAndMatches
FROM matches
WHERE toss_winner=winner
GROUP BY winner
ORDER BY teamWonBothTossAndMatches;

--6. Find a player who has won the highest number of Player of the Match awards for each season

WITH duplicateTable as(SELECT season,player_of_match,count(player_of_match) as count
						FROM matches
						GROUP BY season,player_of_match)
						
SELECT duplicateTable.season,duplicateTable.player_of_match,duplicateTable.count as maxCount
FROM duplicateTable
INNER JOIN (SELECT season,MAX(count) as maxCount
			FROM duplicateTable
            GROUP BY season) as maxCountTable
ON duplicateTable.season = maxCountTable.season AND duplicateTable.count = maxCountTable.maxCount
ORDER BY duplicateTable.season;

--7. Find the strike rate of a batsman for each season

WITH duplicateTable as(SELECT batsman, season , SUM(batsman_runs) as batsmanRuns,(CASE WHEN deliveries.wide_runs = 0  THEN COUNT(deliveries.ball) END) as balls
						FROM deliveries 
						INNER JOIN matches on deliveries.match_id=matches.id
						WHERE deliveries.batsman='V Kohli'
						GROUP BY season,batsman,deliveries.wide_runs)
SELECT  season,batsman,ROUND(CAST(batsmanRuns AS DECIMAL(10,2)) / CAST(balls AS DECIMAL(10,2)),2)*100  as strikeRate
FROM duplicateTable
WHERE ROUND(CAST(batsmanRuns AS DECIMAL(10,2)) / CAST(balls AS DECIMAL(10,2)),2)*100 IS NOT NULL
ORDER BY season;

--8. Find the highest number of times one player has been dismissed by another player

SELECT  bowler,player_dismissed,dismissal_kind, COUNT(player_dismissed) as count
FROM deliveries
GROUP BY bowler,player_dismissed,dismissal_kind
HAVING dismissal_kind !='run out'
ORDER BY count DESC
LIMIT 1;

--9. Find the bowler with the best economy in super overs

WITH superOverData AS
(
    SELECT
        deliveries.bowler AS bowler,
        SUM(deliveries.total_runs - deliveries.legbye_runs - deliveries.bye_runs - deliveries.penalty_runs) AS runs,
        COUNT(CASE WHEN deliveries.wide_runs = 0 AND deliveries.noball_runs = 0 THEN 1 END) as balls
    FROM
        deliveries
    WHERE
        deliveries.is_super_over = 1
    GROUP BY
        deliveries.bowler
)

SELECT
    bowler,
    ROUND(SUM(runs)*6.0 / SUM(balls), 2) AS economy, 
FROM
    superOverData
GROUP BY
    bowler
ORDER BY
    economy ASC
LIMIT 1;